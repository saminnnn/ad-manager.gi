import { Controller, Get, Res } from '@nestjs/common';
import { AppService } from './app.service';
import {Response} from 'express'
import {join} from 'path'

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/dataloader.js')
  getDataLoader(@Res() res: Response) {
    const approot: string = require('app-root-path').toString();
    const pathToFile = join(approot, 'assets', 'dataloader.js')
    res.sendFile(pathToFile);
  }
}
