import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { CommonService } from 'src/common/common.service';
import { AdServerController } from './ad-server/ad-server.controller';
import { FileControllerController } from './file-controller/file-controller.controller';
const busboy = require('connect-busboy');

 
@Module({
  controllers: [FileControllerController, AdServerController],
  providers: [CommonService]
})
export class FileManagerModule implements NestModule{
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(busboy({highWaterMark: 2 * 1024 * 1024})).forRoutes('filewrite/uploadzip');
  }
}
