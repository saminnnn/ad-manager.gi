import { Test, TestingModule } from '@nestjs/testing';
import { AdServerController } from './ad-server.controller';

describe('AdServerController', () => {
  let controller: AdServerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdServerController],
    }).compile();

    controller = module.get<AdServerController>(AdServerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
