import { Controller, Get, Param, Query, Res } from '@nestjs/common';
import { Response } from 'express';
import { CommonService } from 'src/common/common.service';
import {join} from 'path'
 
@Controller()
export class AdServerController {
    constructor(private readonly common: CommonService) {    }
    
    @Get('/serveAd/:adname')
    serveHtml(@Param('adname') adName: string, @Res() res: Response, @Query('version') versionName: string) {
        const filePath = this.getFilePath(adName, 'index.html');
        res.sendFile(filePath);
    }

    @Get('/serveAd/:adname/:assetName')
    serveAssets(@Param('adname') adName: string, @Param('assetName') assetName: string, @Res() res: Response) {
        if(this.common.isFileNameValid(assetName)) {
            const filePath = this.getFilePath(adName, assetName);
            res.sendFile(filePath);
        } else {
            res.send({success: false, message: 'Assetname not valid'});
        }
        
    }

    @Get('/getManifest/:project')
    async getManifest(@Param('project') project: string, @Res() res: Response, @Query('version') version: string){
        console.log(version);
        const manifestPath = join(this.common.getAppRoot(), this.common.getManifestFolderName(), project + '.json');
        res.sendFile(manifestPath);

    }

    getFilePath(adName: string, file: string): string {
        const filePath = this.common.pathToProjectFolder(adName);
        return join(filePath, file);
    }
}
