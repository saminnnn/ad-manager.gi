import { readdir, readFile, writeFile, rm, mkdir } from 'fs/promises';
import {join} from 'path';
import { CommonService } from 'src/common/common.service';

export class ProjectManage {
    private projectRoot = 'ad_projects';
    private approot: string = require('app-root-path').toString();
    private projects: string[];
    private commonService: CommonService;
    

    constructor(commonservice: CommonService) {
        const folderPath = join(this.approot, this.projectRoot);
        readdir(folderPath).then((folders: string[]) => this.projects = folders);
        this.commonService = commonservice;
    }

    createProject(projectName: string): Promise<void> {
        if(this.projects.includes(projectName)) {
            return Promise.reject('Project Exists')
        }

        return this.commonService.createFolder(projectName).then(() => {
            this.projects.push(projectName);
            return Promise.resolve();
        }).catch((e: string) => {
            return Promise.reject(e);
        });
    }

    getProject(): string[] {
        return this.projects;
    }

    deleteProject(project: string, fullProject: boolean): Promise<void> {
        let pathToProject = join(this.approot, this.projectRoot, project);
        return rm(pathToProject, {force: true, maxRetries: 3, recursive: true}).then(() => {
            if(fullProject) {
                const index = this.projects.indexOf(project);
                if(index != -1) {
                    this.projects.splice(index, 1);
                }
                return Promise.resolve();
            } else {
                return mkdir(pathToProject);
            }
               
        });
    }

    writeManifest(manifest: string, project: string): Promise<void> {
        const manifestLines = manifest.split('\r\n');
        const keys = manifestLines[0].split(',');
        const keysLength = keys.length;
        const lines = manifestLines.length;
        const manifestObjects = [];

        for(let i = 1; i < lines; i++) {
            const lineItems = manifestLines[i].split(',');
            const currentRow: any = {};
            for(let ii = 0; ii < keysLength; ii++) {
                if(!lineItems[ii]) lineItems[ii] = '';
                currentRow[keys[ii]] = lineItems[ii];
            }
            manifestObjects.push(currentRow);
        }

        const manifestFile = JSON.stringify(manifestObjects);

        const manifestPath = join(this.commonService.getManifestFolderName(), project + '.json');
        return writeFile(manifestPath, manifestFile, 'utf-8');
    }

    adjustIndexHtml(project: string): Promise<void> {
        const pathToIndex = join(this.commonService.pathToProjectFolder(project), 'index.html');
        return readFile(pathToIndex, 'utf-8').then((value: string) => {
            const indexToHeadTag = value.indexOf('<head');
            const startOfHead = value.indexOf('>', indexToHeadTag) + 1;
            const baseTag = `<base href="` + this.commonService.hostname + `/filewrite/serveAd/` + project + `/" target="_blank">`;

            const output = value.slice(0, startOfHead) + baseTag + value.slice(startOfHead);
            return writeFile(pathToIndex,output, 'utf-8');
        });
    }

    getVersion(version: string, data: string): string {
        const jsonData: any[] = JSON.parse(data);
        const versionData = jsonData.find(e => {
            e.version == version;
        });

        return JSON.stringify(versionData);

    }
}