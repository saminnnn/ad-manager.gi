import { CacheModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileManagerModule } from './file-manager/file-manager.module';
import {RouterModule} from '@nestjs/core'
import { CommonService } from './common/common.service';

 
@Module({
  imports: [
    FileManagerModule, 
    CacheModule.register({isGlobal: true}), 
    RouterModule.register([{path: 'filewrite', module: FileManagerModule}])
  ],
  controllers: [AppController],
  providers: [AppService, CommonService],
})
export class AppModule {}
