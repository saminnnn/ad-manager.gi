var base = document.querySelector('base').href;
var baseLength = base.length;
var projectName = '';

for(i = base.length - 2; i >= 0; i--) {
    var char = base.charAt(i);
    if(base.charAt(i) == '/') break;
    projectName = char + projectName;
}

var adData;

const req = new XMLHttpRequest();
req.addEventListener('load', function() {
    adData = JSON.parse(this.responseText);
    start(adData);
});

var baseLink = 'http://localhost:3000/filewrite/getManifest/';
req.open('GET', baseLink + projectName + window.location.search);
req.send();